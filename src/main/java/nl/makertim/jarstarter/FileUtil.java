package nl.makertim.jarstarter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.util.Date;

public class FileUtil {

	public static Date lastModified(File file) {
		Path path = Paths.get(file.toURI());
		Date lastChanged = new Date(0);
		try {
			FileTime time = Files.getLastModifiedTime(path);
			lastChanged = new Date(time.toMillis());
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return lastChanged;
	}

	public static String fileNameWithoutExtension(File file) {
		String fileName = file == null ? "" : file.getName();
		int i;
		for (i = fileName.length() - 1; i >= 0; i--) {
			if (fileName.charAt(i) == '.') {
				break;
			}
		}
		if (i <= 0) {
			return fileName;
		}
		return fileName.substring(0, i);
	}
}
