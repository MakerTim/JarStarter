package nl.makertim.jarstarter;

import java.sql.Timestamp;
import java.util.Date;

public class JavaStarter implements Starter {

	private final JavaTracker tracker;
	private State state;
	private Process process;
	private Date timeStarted;
	private int lastCode = Integer.MIN_VALUE;

	public JavaStarter(JavaTracker tracker) {
		this.tracker = tracker;
		state = State.OFFLINE;
	}

	@Override
	public State getState() {
		return state;
	}

	@Override
	public void start() {
		if (getState() == State.RESTARTING) {
			return;
		} else if (getState() == State.ONLINE) {
			stop();
		}
		state = State.ONLINE;
		timeStarted = FileUtil.lastModified(tracker.getStartFile());
		String fileName = FileUtil.fileNameWithoutExtension(tracker.getStartFile());
		System.out.println("Starting " + fileName + " from " + new Timestamp(timeStarted.getTime()));
		String prefix = "[" + fileName + "]\t";

		process = RuntimeHelper.start(tracker.getCommand(), tracker.getStartingArgs(), //
				out -> System.out.println(prefix + out), //
				err -> System.out.println(prefix + err), //
				returnCode -> {
					this.lastCode = returnCode;
					state = State.OFFLINE;
					System.out.println("STATUS UPDATE: " + prefix + " is now offline [" + returnCode + "]");
				});
	}

	@Override
	public void stop() {
		if (getState() == State.RESTARTING || getState() == State.OFFLINE) {
			return;
		}
		String fileName = FileUtil.fileNameWithoutExtension(tracker.getStartFile());
		System.out.println("Stopped " + fileName + "version from " + new Timestamp(timeStarted.getTime()));
		process.destroy();
	}

	@Override
	public int lastExitCode() {
		return lastCode;
	}

	@Override
	public Date lastStartDate() {
		return timeStarted;
	}
}
