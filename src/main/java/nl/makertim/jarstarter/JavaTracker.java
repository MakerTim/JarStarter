package nl.makertim.jarstarter;

import java.io.File;

public class JavaTracker implements Tracker<JavaStarter> {

	private File file;
	private String[] args;
	private JavaStarter starter;

	public JavaTracker(File file, String[] args) {
		this.file = file;
		this.args = args;
		this.starter = new JavaStarter(this);
	}

	@Override
	public JavaStarter getStarter() {
		return starter;
	}

	@Override
	public File getStartFile() {
		return file;
	}

	@Override
	public String getCommand() {
		return "java -jar " + getStartFile().getPath();
	}

	@Override
	public String[] getStartingArgs() {
		return args;
	}
}
