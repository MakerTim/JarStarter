package nl.makertim.jarstarter;

import java.io.File;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

	private static ExecutorService threadpool;
	private static Tracker[] trackers;

	public static void main(String[] args) {
		if (args.length == 0) {
			System.err.println("No argument found!");
			help();
			System.exit(1);
		}

		registerTrackers(args);
		threadpool = Executors.newCachedThreadPool();
		startTrackers();
	}

	public static void registerInThreadpool(Runnable runnable) {
		threadpool.submit(runnable);
	}

	private static void startTrackers() {
		Arrays.stream(trackers).forEach(tracker -> threadpool.submit(tracker::startThreaded));
	}

	private static void registerTrackers(String[] args) {
		List<Tracker> trackerList = new ArrayList<>();
		Map<File, String[]> todo = new LinkedHashMap<>();
		for (String nameArgument : args) {
			File file = new File(nameFromNameArgs(nameArgument));
			if (!file.exists()) {
				System.err.println(file.getPath() + " not found");
				continue;
			}
			if (file.isFile()) {
				todo.put(file, argsFromNameArgs(nameArgument));
			}
			if (file.isDirectory()) {
				String[] arguments = argsFromNameArgs(nameArgument);
				Arrays.stream(Objects.requireNonNull(file.listFiles())).forEach(child -> {
					todo.put(child, arguments);
				});
			}
		}
		for (Map.Entry<File, String[]> fileEntry : todo.entrySet()) {
			File file = fileEntry.getKey();
			String[] arguments = fileEntry.getValue();

			Tracker tracker;
			try {
				tracker = TrackerFactory.starterOf(file, arguments);
				trackerList.add(tracker);
			} catch (IllegalArgumentException iae) {
				iae.printStackTrace();
			}
		}
		System.out.println(trackerList.size() + " tracker(s) registered");
		trackers = trackerList.stream().toArray(Tracker[]::new);
	}

	private static String nameFromNameArgs(String arg) {
		return arg.split(" ")[0];
	}

	private static String[] argsFromNameArgs(String arg) {
		String[] fileNameArgs = arg.split(" ");
		String[] args = new String[fileNameArgs.length - 1];
		System.arraycopy(fileNameArgs, 1, args, 0, fileNameArgs.length - 1);
		return args;
	}

	private static void help() {
		System.err.println("\tArguments should only be file names");
		System.err.println("\tLike /opt/service/service.jar");
		System.err.println("\t\t\"/opt/service/service.jar --help\"");
	}

}
