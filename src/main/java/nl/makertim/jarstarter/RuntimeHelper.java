package nl.makertim.jarstarter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.function.Consumer;

public class RuntimeHelper {

	public static Process start(String command, String[] args, Consumer<String> out, Consumer<String> errOut, Consumer<Integer> returnCode) {
		Process process;
		try {
			StringBuilder arguments = new StringBuilder();
			Arrays.stream(args).forEach(arg -> arguments.append(" ").append(arg));
			process = Runtime.getRuntime().exec(command + arguments);
		} catch (Exception ex) {
			ex.printStackTrace();
			returnCode.accept(Integer.MIN_VALUE);
			return null;
		}
		Main.registerInThreadpool(() -> {
			try {
				BufferedReader stdIn = new BufferedReader(new InputStreamReader(process.getInputStream()));
				String buffer;
				while ((buffer = stdIn.readLine()) != null) {
					out.accept(buffer);
				}
			} catch (IOException ex) {
				if (ex.getMessage().equalsIgnoreCase("Stream closed")) {
					return;
				}
				ex.printStackTrace();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});
		Main.registerInThreadpool(() -> {
			try {
				BufferedReader stdErr = new BufferedReader(new InputStreamReader(process.getErrorStream()));
				String buffer;
				while ((buffer = stdErr.readLine()) != null) {
					errOut.accept(buffer);
				}
			} catch (IOException ex) {
				if (ex.getMessage().equalsIgnoreCase("Stream closed")) {
					return;
				}
				ex.printStackTrace();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});
		Main.registerInThreadpool(() -> {
			try {
				returnCode.accept(process.waitFor());
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});
		return process;
	}
}
