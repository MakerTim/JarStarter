package nl.makertim.jarstarter;

import java.util.Date;

public interface Starter {

	enum State {
		ONLINE, OFFLINE, RESTARTING
	}

	State getState();

	void start();

	void stop();

	int lastExitCode();

	Date lastStartDate();
}
