package nl.makertim.jarstarter;

import java.io.File;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public interface Tracker<S extends Starter> {

	S getStarter();

	File getStartFile();

	String[] getStartingArgs();

	default void startThreaded() {
		start();
		while (1 == 1) {
			try {
				TimeUnit.SECONDS.sleep(1);
				if (shouldRestart()) {
					start();
				}
			} catch (InterruptedException ex) {
				ex.printStackTrace();
				break;
			}
		}
	}

	default void start() {
		getStarter().start();
	}

	String getCommand();

	default boolean shouldRestart() {
		File file = getStartFile();
		if (!file.exists()) {
			return false;
		}
		Date lastChanged = FileUtil.lastModified(file);
		return !lastStartTime().equals(lastChanged) || getStarter().getState() == Starter.State.OFFLINE;
	}

	default Date lastStartTime() {
		return getStarter().lastStartDate();
	}

}
