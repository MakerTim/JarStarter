package nl.makertim.jarstarter;

import java.io.File;

public class TrackerFactory {

	public static Tracker<?> starterOf(File file, String[] args) {
		if (file == null) {
			throw new IllegalArgumentException("File cannot be null");
		}
		if (file.isDirectory()) {
			throw new IllegalArgumentException("Directory " + file.getName() + " cannot be started!");
		}
		if (file.getName().endsWith(".jar")) {
			return new JavaTracker(file, args);
		}
		throw new IllegalArgumentException("No Starter for " + file.getName());
	}

}
